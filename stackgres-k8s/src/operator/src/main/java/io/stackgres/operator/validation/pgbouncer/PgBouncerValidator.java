/*
 * Copyright (C) 2019 OnGres, Inc.
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

package io.stackgres.operator.validation.pgbouncer;

import io.stackgres.operator.validation.PgBouncerReview;
import io.stackgres.operatorframework.Validator;

public interface PgBouncerValidator extends Validator<PgBouncerReview> {
}
