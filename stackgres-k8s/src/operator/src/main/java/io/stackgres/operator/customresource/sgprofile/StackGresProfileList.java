/*
 * Copyright (C) 2019 OnGres, Inc.
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

package io.stackgres.operator.customresource.sgprofile;

import io.fabric8.kubernetes.client.CustomResourceList;

public class StackGresProfileList extends CustomResourceList<StackGresProfile> {

  private static final long serialVersionUID = -5276087851826599719L;

}
