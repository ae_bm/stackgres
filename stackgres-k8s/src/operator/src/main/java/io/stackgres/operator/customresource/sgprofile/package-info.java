/*
 * Copyright (C) 2019 OnGres, Inc.
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/**
 * Custom Resource for StackGres resources profile.
 */

package io.stackgres.operator.customresource.sgprofile;
