### Problem to solve

<!-- What problem do we solve? -->

### Further details

<!-- Include use cases, benefits, and/or goals -->

### Proposal

<!-- How could we going to solve the problem? -->

### Testing

<!-- What risks does this change pose? How might it affect the quality of the product?  -->

### Links / references

/label ~"Feature Request" ~StackGres