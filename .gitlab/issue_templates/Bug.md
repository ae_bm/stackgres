<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "bug" label.

Issue tracker:

- https://gitlab.com/ongresinc/stackgres/issues?label_name%5B%5D=Bug

and verify the issue you're about to submit isn't a duplicate.
--->

### Summary

(Summarize the bug encountered concisely)

### Environment

- StackGres version:

- Kubernetes version (use `kubectl version`):

- Cloud provider or hardware configuration:

### Steps to reproduce

(How one can reproduce the issue - this is very important)

### Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)

/label ~Bug ~StackGres